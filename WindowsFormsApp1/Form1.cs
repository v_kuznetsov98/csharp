﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string GetCount(string radiusTxt)
        {
            if (Convert.ToBoolean(radiusTxt.Length))
            {
                var radius = Convert.ToDouble(radiusTxt);
                var result = Math.Round(Math.PI * 2 * radius);

                return Convert.ToString(result);
            }
            else
            {
                return "Введите число.";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label3.Text = GetCount(this.textBox1.Text);
        }

        private void textBox1_TextChanged(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 48 || e.KeyChar >= 59) && e.KeyChar != 8)
                e.Handled = true;
        }
    }
}
