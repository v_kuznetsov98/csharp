﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WindowsFormsApp1.Tests
{
    [TestClass()]
    public class Form1Tests
    {
        [TestMethod()]
        public void InputTwoReturnThirteen()
        {
            Form1 fm = new Form1();
            string expected = "13";
            string arg1 = "2";
            
            string result = fm.GetCount(arg1);
            Assert.AreEqual(expected, result);
        }

        [TestMethod()]
        public void InputEmptyReturnMessage()
        {
            Form1 fm = new Form1();
            string expected = "Введите число.";
            string arg1 = "";

            string result = fm.GetCount(arg1);
            Assert.AreEqual(expected, result);
        }
    }
}